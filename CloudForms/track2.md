# Reports, Control Policies, Alerts - Build and Document Examples

We are curating and adding more content to CloudForms so that we have more:

* Reports
* Dashboards
* Policies
* Policy Profiles
* Alerts
* Alert Profiles

And, we can add them to the product. 

Many people were using [Ramrexx versions](https://github.com/ramrexx?tab=repositories), but those are no longer maintained by Kevin.

We have several already in the [COP GitLab repository](https://gitlab.com/redhat-cop/mbu-lab/cloudforms).

You can find [more information about this](https://gitlab.com/redhat-cop/EMEACloudFormsHackathon/introduction/issues/33) from previous hackathons.